import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";
import AppNav from "./AppNavigator";
import store from "./redux/store";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center"
  }
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Provider store={store}>
        <AppNav />
      </Provider>
    );
  }
}

export default App;
