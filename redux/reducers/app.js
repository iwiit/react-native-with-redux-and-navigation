import actions from "../actions";

var initialState = {
  text: "Hello World",
  textColor: "black",
  textSize: "14px"
};

export default function textReducer(state = initialState, action) {
  switch (action.type) {
    case actions.TEXT_CHANGE:
      return {
        ...state,
        text: action.payload
      };
    case actions.TEXT_SIZE_CHANGE:
      return {
        ...state,
        textSize: action.payload
      };

    default:
      return state;
  }
}
