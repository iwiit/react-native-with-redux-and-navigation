import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";

import SignIn from "./components/signin/index";
import Home from "./components/home/index";

const AppNavigator = createStackNavigator({
  SignIn: { screen: SignIn, path: "signin" },
  Home: { screen: Home, path: "home" }
});

const AppNav = createAppContainer(AppNavigator);

export default AppNav;
