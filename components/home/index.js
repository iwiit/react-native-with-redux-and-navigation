import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Provider } from "react-redux";
import { connect } from "react-redux";
import actions from "./../../redux/actions";

const { textChange } = actions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center"
  }
});

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  changeText = () => {
    const { inputText, textChange } = this.props;
    textChange(inputText === "Goodbye World" ? "Hello World" : "Goodbye World");
  };

  render() {
    const { inputText } = this.props;

    return (
      <View style={styles.container}>
        <Text onClick={this.doSomething}>Home</Text>
        <Button onPress={this.changeText} title="Change Text" />
      </View>
    );
  }
}

export default connect(
  state => ({
    ...state.App,
    inputText: state.TextProps.text
  }),
  {
    textChange
  }
)(Home);
